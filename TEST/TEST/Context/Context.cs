﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TEST.Models;

namespace TEST.Context
{
    public class CategoryContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
    }

    public class ProductContext : DbContext
    {
        public DbSet<Product> Products { get; set; }


    }
}