﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DemoRESTServiceCRUD
{
    [DataContract]
    public class Book
    {
        [DataMember]
        public int BookId { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
    }
    
    public interface IBookRepository
    {
        List<Book> GetAllbooks();
        Book GetBookById(int id);
        Book AddNewBook(Book item);
        bool DeleteABook(int id);
        bool UpdateABook(Book item);
    }

    public class BookRepository : IBookRepository
    {
        private List<Book> books = new List<Book>();
        private int counter = 1;

        public BookRepository()
        {
            AddNewBook(new Book { Title = "C# Programming", ISBN = "123456789" });
            AddNewBook(new Book { Title = "Java Programming", ISBN = "987654321" });
            AddNewBook(new Book { Title = "WCF Programming", ISBN = "1341343484" });
        }
        //CRUD Operations
        //1. CREATE
        public Book AddNewBook(Book newBook)
        {
            if (newBook == null)
                throw new ArgumentException("newBook");
            newBook.BookId = counter++;
            books.Add(newBook);
            return newBook;
        }

        //2. RETRIEVE ALL
        public List<Book> GetAllbooks()
        {
            return books;
        }
        //3. RETRIEVE By BookId
        public Book GetBookById(int bookId)
        {
            return books.Find(b => b.BookId == bookId);
        }
        //4. UPDATE
        public bool UpdateABook(Book updateBook)
        {
            if (updateBook == null)
                throw new ArgumentException("updateBook");

            int idx = books.FindIndex(b => b.BookId == updateBook.BookId);
            if (idx == -1)
                return false;
            books.RemoveAt(idx);
            books.Add(updateBook);
            return true;

         }


        public bool DeleteABook(int bookId)
        {
            int idx = books.FindIndex(b => b.BookId == bookId);
            if (idx == -1)
                return false;
            books.RemoveAll(b => b.BookId == bookId);
            return true;
        }
    }
}